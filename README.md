# Jousen ja pallon simulaatio

Ohjelmalla voi simuloida jousen ja pallon luoman systeemin liikkeitä. Vasemmalla puolella on muutama kirjoituskenttä, joihin on tarkoitus kirjoittaa eri pallon ja jousen muuttujia. Kun muuttujat on syötetty halutuiksi, painetaan "aloita"-nappi, jolloin simulaatio alkaa oikealla puolella olevassa laatikossa. Uuden simulaation voi aloittaa muuttamalla muuttujien arvoja ja painamalla uudelleen "aloita"-nappia. Mahdolliset muuttujat ovat selviä nimiensä perusteella, mutta tässä vielä ne selitetty.

Muuttujan nimi    | Selitys
------------------|------------------------------------------------------------
Pallon massa      | Pallon massa. (kilogrammoina)
Pallon x-sijainti | Pallon x-koordinaatti alussa. (prosentteina)
Pallon y-sijainti | Pallon y-koordinaatti alussa. (prosentteina)
Pallon x-nopeus   | Pallon x-suuntainen nopeus alussa. (metriä sekunnissa)
Pallon y-nopeus   | Pallon y-suuntainen nopeus alussa. (metriä sekunnissa)
Jousivakio        | Jousen jousivakio. (N/m)
