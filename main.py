from tkinter import *


class Vektori:
    def __init__(self, x, y):
        """
        2D vektorin rakentaja.

        :param x: vektorin x-komponentti (float)
        :param y: vektorin y-komponentti (float)
        """
        self.x = x
        self.y = y

    def __add__(self, b):
        """
        Mahdollisuus summata vektoreita.

        :param b: summattava (Vektori)
        :return: summa (Vektori)
        """
        return Vektori(self.x + b.x, self.y + b.y)

    def __radd__(self, b):
        """
        Mahdollisuus summata myös toisin päin.

        :param b: summattava (Vektori)
        :return: summa (Vektori)
        """
        return self + b

    def __sub__(self, b):
        """
        Mahdollisuus laskea vektoreiden erotus.

        :param b: erotettava (Vektori)
        :return: erotus (Vektori)
        """
        return self + -1 * b

    def __rsub__(self, b):
        """
        Mahdollisuus laskea erotus myös toisin päin.

        :param b: erotettava (Vektori)
        :return: erotus (Vektori)
        """
        return -1 * self + b

    def __mul__(self, b):
        """
        Mahdollisuus laske kertolaskua vektoreilla.

        :param b: kerrottava (Vektori|float)
        :return: tulo (Vektori)
        """
        if isinstance(b, Vektori):
            return Vektori(self.x * b.x, self.y * b.y)
        return Vektori(self.x * b, self.y * b)

    def __rmul__(self, b):
        """
        Mahdollisuus laskea kertolasku myös toisin päin.

        :param b: kerrottava (Vektori|float)
        :return: tulo (Vektori)
        """
        return self * b

    def __truediv__(self, b):
        """
        Mahdollisuus laske jakolasku vektorin kanssa.

        :param b: jakaja (b)
        :return: osamäärä (Vektori)
        """
        return self * (1 / b)

    def pituus(self):
        """
        Antaa vektorin pituuden.

        :return: vektorin pituus (float)
        """
        return (self.x ** 2 + self.y ** 2) ** 0.5


class DPICanvas:
    def __init__(self, canvas, sivun_pituus, dpi):
        """
        DPI canvaan rakentaja.

        :param canvas: canvas, jolle piirretään (Canvas)
        :param sivun_pituus: canvaan sivun pituus pikseleinä (int)
        :param dpi: "dots per inch" -arvo piirtämiseen (float)
        """
        self.__canvas = canvas
        self.__sivun_pituus = sivun_pituus
        self.__dpi = dpi

    def __pikseleina(self, arvo_metreina):
        """
        Muuttaa metriset arvot pikseleiksi.

        :param arvo_metreina: metrinen arvo, joka muutetaan (float)
        :return: arvo pikseleinä (float)
        """
        return arvo_metreina / 0.0254 * self.__dpi

    def __metreina(self, arvo_pikseleina):
        """
        Muuttaa pikselit metreiksi.

        :param arvo_pikseleina: arvo pikseleinä (float)
        :return: arvo metreinä (float)
        """
        return arvo_pikseleina / self.__dpi * 0.0254

    def rel(self, prosentti):
        """
        Muuttaa prosenttiluvun metriseksi.

        :param prosentti: prosenttiluku, joka muutetaan (float)
        :return: pituus metreinä (float)
        """
        pikselit = self.__sivun_pituus * prosentti / 100
        return self.__metreina(pikselit)

    def viiva(self, alku, loppu, paksuus=3, vari='#636363'):
        """
        Piirtää viivan canvaalle.

        :param alku: viivan aloituspiste (Vektori)
        :param loppu: viivan lopetuspiste (Vektori)
        :param paksuus: viivan paksuus (float)
        :param vari: viivan väri (str)
        :return: tkinterin palauttama canvas objekti
        """
        x1, y1 = self.__pikseleina(alku.x), self.__pikseleina(alku.y)
        x2, y2 = self.__pikseleina(loppu.x), self.__pikseleina(loppu.y)

        return self.__canvas.create_line(
            x1, y1, x2, y2, width=paksuus, fill=vari)

    def ympyra(self, piste, sade, vari='#636363'):
        """
        Piirtää ympyrän canvaalle.

        :param piste: ympyrän keskipiste (Vektori)
        :param sade: ympyrän säde (float)
        :param vari: ympyrän väri (str)
        :return: tkinterin palauttama canvas objekti
        """
        x, y = self.__pikseleina(piste.x), self.__pikseleina(piste.y)
        sade = self.__pikseleina(sade)

        return self.__canvas.create_oval(
            x - sade, y - sade, x + sade, y + sade, fill=vari, outline='')

    def poista(self, objekti):
        """
        Poistaa yhden objektin canvaalta.

        :param objekti: poistettava objekti
        """
        self.__canvas.delete(objekti)

    def tyhjenna(self):
        """
        Tyhjentää koko canvaan.
        """
        self.__canvas.delete(ALL)

    def koko(self):
        """
        Antaa canvaan koon metreinä.

        :return: canvaan koko (float)
        """
        return self.__metreina(self.__sivun_pituus)


class Pallo:
    def __init__(self, sijainti, massa):
        """
        Pallon rakentaja.

        :param sijainti: pallon alkusijainti (Vektori)
        :param massa: pallon massa (float)
        """
        self.__canvas_objektit = []
        self.__massa = massa
        self.__kimmokerroin = 0.33
        self.__painovoima = Vektori(0, self.__massa * 9.81)
        self.sade = 0.07
        self.sijainti = sijainti
        self.nopeus = Vektori(0, 0)

    def __hidastava_voima(self):
        """
        Laskee palloa hidastavan voiman.

        :return: voima, joka tällä hetkellä hidastaa palloa (Vektori)
        """
        if self.nopeus.pituus() == 0:
            return Vektori(0, 0)

        kiihtyvyys = 2.2 * self.nopeus.pituus()
        yksikkovektori = -1 * self.nopeus / self.nopeus.pituus()

        return self.__massa * kiihtyvyys * yksikkovektori

    def kimpoa_suuntaan(self, akseli, suunta):
        """
        Aseta pallo kimpoamaan tiettyyn suuntaan.

        :param akseli: akseli, jolla kimpoaminen tapahtuu (str)
        :param suunta: kimpoamisen suunta (int)
        """
        if akseli == 'x':
            if (self.nopeus.x < 0 and suunta == 1) or \
                    (self.nopeus.x > 0 and suunta == -1):
                self.nopeus.x *= -1 * self.__kimmokerroin
        elif akseli == 'y':
            if (self.nopeus.y < 0 and suunta == 1) or \
                    (self.nopeus.y > 0 and suunta == -1):
                self.nopeus.y *= -1 * self.__kimmokerroin

    def paivita(self, aika, voima):
        """
        Päivitä pallon sijainti.

        :param aika: aika edellisestä päivityksestä (float)
        :param voima: palloon vaikuttava voima (Vektori)
        """
        aika /= 1000
        kokonaisvoima = self.__hidastava_voima() + self.__painovoima + voima

        self.sijainti += self.nopeus * aika
        self.nopeus += kokonaisvoima / self.__massa * aika

    def piirra(self, canvas):
        """
        Piirrä pallo näkyviin.

        :param canvas: canvas, johon pallo piirretään (DPICanvas)
        """
        while len(self.__canvas_objektit) > 0:
            canvas.poista(self.__canvas_objektit.pop(0))

        self.__canvas_objektit.append(
            canvas.ympyra(self.sijainti, self.sade, '#2e333c'))


class Jousi:
    def __init__(self, keskipiste, jousivakio):
        """
        Jousen rakentaja.

        :param keskipiste: jousen pidikkeen sijainti (Vektori)
        :param jousivakio: jousen jousivakio (float)
        """
        self.__canvas_objektit = []
        self.__keskipiste = keskipiste
        self.__jousivakio = jousivakio
        self.__neutraali_pituus = 0.18
        self.__pituus = Vektori(0, 0)
        self.voima = Vektori(0, 0)

    def __pituus_delta(self):
        """
        Laskee jousen pituuden sen neutraalin pituuden suhteen.

        :return: jousen jännittynyt pituus (Vektori)
        """
        pituus = self.__pituus.pituus()
        return (pituus - self.__neutraali_pituus) / pituus * self.__pituus

    def paivita(self, pituus):
        """
        Päivitä jousen pituus.

        :param pituus: jousen uusi pituus (Vektori)
        """
        self.__pituus = pituus - self.__keskipiste
        self.voima = -1 * self.__jousivakio * self.__pituus_delta()

    def piirra(self, canvas):
        """
        Piirrä jousi canvaalle.

        :param canvas: canvas, johon piirretään (DPICanvas)
        """
        while len(self.__canvas_objektit) > 0:
            canvas.poista(self.__canvas_objektit.pop(0))

        self.__canvas_objektit.append(
            canvas.viiva(self.__keskipiste,
                         self.__keskipiste + self.__pituus, 3, '#d7d7d7'))

        self.__canvas_objektit.append(
            canvas.ympyra(self.__keskipiste, 0.03, '#b9b9b9'))


class Simulaatio:
    def __init__(self):
        """
        Simulaation rakentaja.
        """
        self.__paivitystaajuus = 12
        self.__tk = Tk()
        self.__pallo = None
        self.__jousi = None

        self.__tk.title('Jousi simulaatio')

        self.__muuttujien_nimet = [
            'massa', 'x-sijainti', 'y-sijainti',
            'x-nopeus', 'y-nopeus', 'jousivakio'
        ]
        self.__muuttujien_tarkistus = [
            {'min': 1, 'max': 100},
            {'min': 0, 'max': 100},
            {'min': 0, 'max': 100},
            {'min': -20, 'max': 20},
            {'min': -20, 'max': 20},
            {'min': 0, 'max': 1000}
        ]
        self.__muuttujien_alkuarvot = [6, 92, 62, -5.8, -6.2, 258]

        self.__alusta_kayttoliittyma()
        self.__tayta_alkuarvot()

    def __alusta_kayttoliittyma(self):
        """
        Alustaa käyttöliittymän.
        """

        # Jaa käyttöliittymä puoliksi
        valinnat_frame = Frame(self.__tk)
        canvas_frame = Frame(self.__tk)

        valinnat_frame.grid(row=0, column=0, padx=10, pady=10, sticky=N)
        canvas_frame.grid(row=0, column=1, padx=(0, 10), pady=10)

        # Lisää syötettävien arvojen labelit
        muuttujien_labelit = [
            'Pallon massa (kg):', 'Pallon x-sijainti (%):',
            'Pallon y-sijainti (%):', 'Pallon x-nopeus (m/s):',
            'Pallon y-nopeus (m/s):', 'Jousivakio (N/m):'
        ]

        for i in range(len(muuttujien_labelit)):
            Label(valinnat_frame, text=muuttujien_labelit[i])\
                .grid(row=i, column=0, sticky=W)

        # Lisää syötettävien arvojen entryt
        self.__muuttujien_entryt = [
            Entry(valinnat_frame) for i in range(len(self.__muuttujien_nimet))
        ]

        for i in range(len(self.__muuttujien_entryt)):
            self.__muuttujien_entryt[i].grid(row=i, column=1)

        # Lisää aloita- ja lopeta-nappulat
        Button(valinnat_frame, text='aloita', command=self.__luo_simulaatio)\
            .grid(row=8, column=0, columnspan=2, sticky=W+E)
        Button(valinnat_frame, text='lopeta', command=self.__lopeta)\
            .grid(row=9, column=0, columnspan=2, sticky=W+E)

        # Lisää virheilmoitus
        self.__virheilmoitus = Label(valinnat_frame, foreground='red')
        self.__virheilmoitus.grid(row=10, column=0, columnspan=2)

        # Lisää canvas
        canvas = Canvas(canvas_frame, width=520, height=520,
                        highlightthickness=1, highlightbackground='#a3a1a1')
        canvas.grid(row=0, column=0)

        self.__canvas = DPICanvas(canvas, 520, 8.8)

    def __tayta_alkuarvot(self):
        """
        Täyttää muuttujiin alkuarvot.
        """
        for i in range(len(self.__muuttujien_entryt)):
            entry = self.__muuttujien_entryt[i]
            entry.insert(0, self.__muuttujien_alkuarvot[i])

    def __tarkista_muuttuja(self, indeksi, arvo):
        """
        Tarkistaa käyttäjän syöttämän arvon.

        :param indeksi: arvon indeksi muuttujissa (int)
        :param arvo: käyttäjän syöttämä arvo (str)
        :return: tieto, onko sallittu (boolean)
        """
        tarkistus = self.__muuttujien_tarkistus[indeksi]

        if ('min' in tarkistus and arvo < tarkistus['min']) or \
           ('max' in tarkistus and arvo > tarkistus['max']):
            return False

        return True

    def __luo_virheilmoitus(self, indeksi):
        """
        Luo tietylle muuttujalle virheilmoituksen.

        :param indeksi: muuttujan indeksi (int)
        :return: virheilmoitus (str)
        """
        nimi = self.__muuttujien_nimet[indeksi]
        tarkistus = self.__muuttujien_tarkistus[indeksi]
        virhe = '{:s} arvo pitää olla {:s}'
        loppuosa = None

        if 'max' in tarkistus and 'min' in tarkistus:
            loppuosa = 'välillä {:d}-{:d}'\
                .format(tarkistus['min'], tarkistus['max'])
        elif 'min' in tarkistus:
            loppuosa = 'yli {:d}'.format(tarkistus['min'])
        elif 'max' in tarkistus:
            loppuosa = 'alle {:d}'.format(tarkistus['max'])

        if not loppuosa:
            return '"{:s}" arvo on virheellinen'.format(nimi)
        else:
            return virhe.format(nimi, loppuosa)

    def __tulosta_virhe(self, ilmoitus=None):
        """
        Tulostaa virheilmoituksen näytölle.

        :param ilmoitus: tulostettava virhe (str)
        """
        virheteksti = ''

        if ilmoitus:
            virheteksti = 'Virhe: {:s}.'.format(ilmoitus)

        self.__virheilmoitus.config(text=virheteksti)

    def __lue_arvot(self):
        """
        Lukee käyttäjän syötteet.
        """
        self.__tulosta_virhe()

        try:
            arvot = []

            # Käy kaikki entryt läpi
            for i in range(len(self.__muuttujien_tarkistus)):
                arvo = float(self.__muuttujien_entryt[i].get())

                # Näytä virheilmoitus ja lopeta lukeminen, jos virheellinen
                if not self.__tarkista_muuttuja(i, arvo):
                    self.__tulosta_virhe(self.__luo_virheilmoitus(i))
                    return None

                arvot.append(arvo)

            return arvot
        except ValueError:
            self.__tulosta_virhe(
                'kaikki antamasi luvut eivät ole desimaalilukuja')
            return None

    def __luo_simulaatio(self):
        """
        Luo uuden pallon ja jousen uusilla arvoilla.
        """
        arvot = self.__lue_arvot()

        if arvot:
            koko = self.__canvas.koko()
            self.__canvas.tyhjenna()

            massa = arvot[0]
            sijainti_x = self.__canvas.rel(arvot[1])
            sijainti_y = self.__canvas.rel(arvot[2])
            sijainti = Vektori(sijainti_x, sijainti_y)
            nopeus = Vektori(arvot[3], arvot[4])
            jousivakio = arvot[5]

            # Luodaan uusi pallo
            self.__pallo = Pallo(sijainti, massa)
            self.__pallo.sijainti *= (koko - 2 * self.__pallo.sade) / koko
            self.__pallo.sijainti += Vektori(
                    self.__pallo.sade, self.__pallo.sade)
            self.__pallo.nopeus = nopeus

            # Luodaan uusi jousi
            self.__jousi = Jousi(
                Vektori(self.__canvas.rel(50), 0.5), jousivakio)

    def __paivita(self):
        """
        Päivittää pallon ja jousen sijainnit, sekä tarkistaa törmäykset.
        """
        if not self.__pallo or not self.__jousi:
            self.__tk.after(self.__paivitystaajuus, self.__paivita)
            return

        # Tunnista törmäykset
        pallo_x = self.__pallo.sijainti.x
        pallo_y = self.__pallo.sijainti.y

        if pallo_x < self.__pallo.sade:
            self.__pallo.kimpoa_suuntaan('x', 1)
        elif pallo_x > self.__canvas.koko() - self.__pallo.sade:
            self.__pallo.kimpoa_suuntaan('x', -1)

        if pallo_y < self.__pallo.sade:
            self.__pallo.kimpoa_suuntaan('y', 1)
        elif pallo_y > self.__canvas.koko() - self.__pallo.sade:
            self.__pallo.kimpoa_suuntaan('y', -1)

        # Päivitä pallon ja jousen sijainti, ja piirrä ne
        self.__pallo.paivita(self.__paivitystaajuus, self.__jousi.voima)
        self.__jousi.paivita(self.__pallo.sijainti)
        self.__jousi.piirra(self.__canvas)
        self.__pallo.piirra(self.__canvas)

        self.__tk.after(self.__paivitystaajuus, self.__paivita)

    def __lopeta(self):
        """
        Sammuta ikkuna.
        """
        self.__tk.destroy()

    def kaynnista(self):
        """
        Käynnistä ikkuna.
        """
        self.__tk.after(self.__paivitystaajuus, self.__paivita)
        self.__tk.mainloop()


def main():
    simulaatio = Simulaatio()
    simulaatio.kaynnista()


main()
